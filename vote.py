from face_classification import *

def voting(data, new_elements, parameters):
	methods = [get_histogram, get_dft, get_dct, get_gradient, get_scale]
	res = {}
	for method in methods:
		res[method.__name__] = classifier(data, new_elements, method, parameters[method.__name__])
	tmp = []
	for i in range(len(new_elements)):
		temp = {}
		for method in res:
			t = res[method][i]
			if t in temp:
				temp[t] += 1
			else:
				temp[t] = 1
		best_size = sorted(temp.items(), key=lambda item: item[1], reverse=True)[0]
		tmp.append(best_size[0])
	return tmp

def test_voting(train, test, parameters):
	res = voting(train, test[0], parameters)
	sum = 0
	for i in range(len(test[0])):
		if test[1][i] == res[i]:
			sum += 1
	return sum/len(test[0])


def vote_classifier(data):
	parameters, train_size = cross_validation(data)
	x_train, y_train, x_test, _ = split_data(data, train_size)
	train = mesh_data([x_train, y_train])
	return voting(train, x_test, parameters)

def cross_validation(data):
	methods = [get_histogram, get_dft, get_dct, get_gradient, get_scale]
	res = []
	start = 5
	end = 8
	for size in range(start, end):
		print(str(size) + " face from class")
		X_train, X_test, y_train, y_test = split_data(data, size)
		train = mesh_data([X_train, y_train])
		test = mesh_data([X_test, y_test])
		parameters = {}
		for method in methods:
			print(method.__name__)
			parameters[method.__name__] = teach_parameter(train, test, method)[0][0]
		print(parameters)
		classf = test_voting(train, test, parameters)
		print(classf)
		res.append([parameters, classf])

	best_res = [[], 0]
	best = 0
	for i in range(start, end):
		if res[i - start][1] > best:
			best = res[i - start][1]
			best_res[0] = res[i - start][0]
			best_res[1] = i
	best_res.append(best)
	return best_res

def cross_validation_graphs(data, method, folds=3):
	if folds < 3:
		folds = 3
	per_fold = int(len(data[0])/folds)
	x_train = []
	x_test = []
	y_train = []
	y_test = []
	results = []
	for step in range(0, folds):
		print("fold " + str(step))
		if step == 0:
			x_train = data[0][per_fold:]
			x_test = data[0][:per_fold]
			y_train = data[1][per_fold:]
			y_test = data[1][:per_fold]
		else:
			if step == folds - 1:
				x_train = data[0][:step*per_fold]
				x_test = data[0][step*per_fold:]
				y_train = data[1][:step*per_fold]
				y_test = data[1][step*per_fold:]
			else:
				x_train = data[0][:step*per_fold] + data[0][(step+1)*per_fold:]
				x_test = data[0][step*per_fold:(step+1)*per_fold]
				y_train = data[1][:step*per_fold] + data[1][(step+1)*per_fold:]
				y_test = data[1][step*per_fold:(step+1)*per_fold]
		results.append(teach_parameter([x_train, y_train], [x_test, y_test], method))
	res = results[0]
	for element in results[1:]:
		best = element[0]
		stat = element[1]
		res[0][0] += best[0]
		res[0][1] += best[1]
		for i in range(len(stat[1])):
			res[1][1][i] += stat[1][i]
	res[0][0] /= folds
	if method != get_scale:
		res[0][0] = int(res[0][0])
	res[0][1] /= folds
	for i in range(len(res[1][1])):
		res[1][1][i] /= folds
	return res